import React, { Dispatch, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import SettingsActions from '../../state/actions/settings-actions';

export const Settings: React.FC = () => {
	const dispatch: Dispatch<any> = useDispatch();

	useEffect(() => {
		dispatch(SettingsActions.fetchSettingsData('temp_id'));
	}, [dispatch]);
	return <>Settings</>;
};
