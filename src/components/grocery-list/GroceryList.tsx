import React, { Dispatch, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import GroceryListActions from '../../state/actions/grocery-list-actions';

export const GroceryList: React.FC = () => {
	const dispatch: Dispatch<any> = useDispatch();

	useEffect(() => {
		dispatch(GroceryListActions.fetchGroceryListData('temp_id'));
	}, [dispatch]);
	return <>Grocery</>;
};
