import React, { Dispatch, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import RecipesActions from '../../state/actions/recipes-actions';

export const Recipes: React.FC = () => {
	const dispatch: Dispatch<any> = useDispatch();

	useEffect(() => {
		dispatch(RecipesActions.fetchRecipesData('temp_id'));
	}, [dispatch]);
	return <>Recipes</>;
};
