import React, { Dispatch, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import MealPlanActions from '../../state/actions/meal-plan-actions';
import Calendar from 'react-calendar';

export const Plan: React.FC = () => {
	const dispatch: Dispatch<any> = useDispatch();

	useEffect(() => {
		dispatch(MealPlanActions.fetchMealPlanData('temp_id'));
	}, [dispatch]);
	return (
		<>
			<Calendar />
		</>
	);
};
