import React from 'react';
import 'bootstrap';

interface NavMenuProps {}

const NavBar: React.FC<NavMenuProps> = (props: NavMenuProps) => {
	return (
		<nav className="navbar fixed-bottom navbar-expand navbar-dark bg-dark">
			<ul className="navbar-nav mx-auto">
				<li className="nav-item">
					<a className="nav-link" href="/meals">
						Recipes
					</a>
				</li>
				<li className="nav-item">
					<a className="nav-link" href="/calendar">
						Meal Plan
					</a>
				</li>
				<li className="nav-item">
					<a className="nav-link" href="/grocery">
						Grocery List
					</a>
				</li>
				{/*<li className="nav-item">*/}
				{/*	<a className="nav-link" href="/settings">*/}
				{/*		Settings*/}
				{/*	</a>*/}
				{/*</li>*/}
			</ul>
		</nav>
	);
};

export default NavBar;
