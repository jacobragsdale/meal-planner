import { ACTION_TYPES } from '../action-types';

const initialState = {};

export type ActionType = {
	type: string;
	payload?: any;
};

export const mealsReducer = (
	state: {} = initialState,
	action: ActionType
): typeof initialState | undefined => {
	switch (action.type) {
		case ACTION_TYPES.STORE_RECIPES_DATA:
			return {
				...state,
				mealData: action.payload,
			};
		default:
			return state;
	}
};
