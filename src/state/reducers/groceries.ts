import { ACTION_TYPES } from '../action-types';
import { GroceryItem } from '../mock-data/grocery/mock-grocery';

const initialState = {};

export type ActionType = {
	type: string;
	payload?: GroceryItem[];
};

export const groceriesReducer = (
	state: {} = initialState,
	action: ActionType
): typeof initialState | undefined => {
	switch (action.type) {
		case ACTION_TYPES.STORE_GROCERY_LIST_DATA:
			return {
				...state,
				groceryData: action.payload,
			};
		default:
			return state;
	}
};
