import { combineReducers } from '@reduxjs/toolkit';
import { mealsReducer } from './meals';
import { groceriesReducer } from './groceries';
import { commonReducer } from './common';
import { settingsReducer } from './settings';

const rootReducer = combineReducers({
	common: commonReducer,
	meals: mealsReducer,
	groceries: groceriesReducer,
	settings: settingsReducer,
});

export default rootReducer;
