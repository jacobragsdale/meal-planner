import { ACTION_TYPES } from '../action-types';

const initialState = {};

export type ActionType = {
	type: string;
	payload?: any;
};

export const calendarReducer = (
	state: {} = initialState,
	action: ActionType
): typeof initialState | undefined => {
	switch (action.type) {
		case ACTION_TYPES.STORE_MEAL_PLAN_DATA:
			return {
				...state,
				calendarData: action.payload,
			};
		default:
			return state;
	}
};
