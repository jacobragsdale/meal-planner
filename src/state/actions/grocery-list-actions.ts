import { ACTION_TYPES } from '../action-types';

export default class GroceryListActions {
	static storeGroceryListData(data: any) {
		return {
			type: ACTION_TYPES.STORE_GROCERY_LIST_DATA,
			payload: data,
		};
	}

	static fetchGroceryListData = (id: string) => (dispatch: any) => {
		//api calls here
		let mockGroceryData = { test_id: id };
		dispatch(GroceryListActions.storeGroceryListData(mockGroceryData));
	};
}
