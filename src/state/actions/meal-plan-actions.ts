import { ACTION_TYPES } from '../action-types';

export default class MealPlanActions {
	static storeMealPlanData(data: any) {
		return {
			type: ACTION_TYPES.STORE_MEAL_PLAN_DATA,
			payload: data,
		};
	}

	static fetchMealPlanData = (id: string) => (dispatch: any) => {
		//api calls here
		let mockGroceryData = { test_id: id };
		dispatch(MealPlanActions.storeMealPlanData(mockGroceryData));
	};
}
