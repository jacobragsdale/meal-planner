import { ACTION_TYPES } from '../action-types';

export default class CommonActions {
	static updateActiveContext = (newContext: string) => {
		return {
			type: ACTION_TYPES.UPDATE_CONTEXT,
			payload: newContext,
		};
	};
}
