import { ACTION_TYPES } from '../action-types';

export default class RecipesActions {
	static storeRecipesData(data: any) {
		return {
			type: ACTION_TYPES.STORE_RECIPES_DATA,
			payload: data,
		};
	}

	static fetchRecipesData = (id: string) => (dispatch: any) => {
		//api calls here
		let mockMealData = { test_id: id };
		dispatch(RecipesActions.storeRecipesData(mockMealData));
	};
}
