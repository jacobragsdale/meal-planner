import { ACTION_TYPES } from '../action-types';

export default class SettingsActions {
	static storeSettingsData(data: any) {
		return {
			type: ACTION_TYPES.STORE_SETTINGS_DATA,
			payload: data,
		};
	}

	static fetchSettingsData = (id: string) => (dispatch: any) => {
		//api calls here
		let mockSettingsData = { test_id: id };
		dispatch(SettingsActions.storeSettingsData(mockSettingsData));
	};
}
