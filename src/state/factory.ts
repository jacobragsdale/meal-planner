import { applyMiddleware, createStore } from '@reduxjs/toolkit';
import rootReducer from './reducers/root';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

const store = createStore(
	rootReducer,
	composeWithDevTools(applyMiddleware(thunk))
);

export { store };
