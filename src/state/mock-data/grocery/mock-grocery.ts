export type GroceryItem = {
	name: string;
	quantity: number;
	upc: string;
};

export const MockGrocery: GroceryItem[] = [
	{
		name: 'chicken noodle soup',
		quantity: 2,
		upc: 'abc123',
	},
	{
		name: 'tylenol pm',
		quantity: 10,
		upc: 'abc124',
	},
];
