import React from 'react';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from './components/common/navbar/NavBar';
import { Provider } from 'react-redux';
import { store } from './state/factory';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Recipes } from './components/recipes/Recipes';
import { GroceryList } from './components/grocery-list/GroceryList';
import { Settings } from './components/settings/Settings';
import { Plan } from './components/meal-plan/MealPlan';

//3rd Party Package Stylings
import 'react-calendar/dist/Calendar.css';

function App() {
	return (
		<>
			<Provider store={store}>
				<Router>
					<NavBar />
					<Route path="/meals" component={Recipes} />
					<Route path="/calendar" component={Plan} />
					<Route path="/grocery" component={GroceryList} />
					<Route path="/settings" component={Settings} />
				</Router>
			</Provider>
		</>
	);
}

export default App;
